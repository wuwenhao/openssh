openSSH
====================

#说明

 本程序使用tornado 编写的ssh代理程序,可以将ssh转化成websocket协议,可以集成到你想要的系统中

#使用

安装依赖

pip install -r 项目/requirements.txt

运行

python main.py -log_file_prefix=openssh.log -port=9527

更多配置请看项目下openssh.conf

启动后访问http://127.0.0.1:9527
#已知问题

目标机器如果不是utf-8编码,会出现中文编码问题

# 效果展示
<img src="http://image18.poco.cn/mypoco/myphoto/20161230/14/17479142020161230141529021.gif?1440x900_110">
<img src="http://image18.poco.cn/mypoco/myphoto/20161230/14/17479142020161230140212045.gif?1440x900_110">
